# Olá, bem vindo a avaliação técnica para o posto Fullstack developer da FutFanatics

Para comerçarmos faça o clone desse repositório para sua maquina, e leia atentamente o documento contendo as instruções.

- Antes de fazer o commit certifique-se que tenha finalizado corretamente, somente será válido o primeiro commit;
- Crie uma branch com seu nome e faça commit nela, não será aceito commits na branch principal;